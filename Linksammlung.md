# Linksammlung
* [DBIS: Fachübersicht (uni-regensburg.de)](https://rzblx10.uni-regensburg.de/dbinfo/fachliste.php?bib_id=bthac&lett=l&colors=&ocolors=)
* Citavi: https://www.fb3.rwth-aachen.de/cms/Bauingenieurwesen/Studium/Im-Studium/~hfod/Leitfaden-wissenschaftliche-studentische/
* Fit für die Bib: https://www.ub.rwth-aachen.de/go/id/raut/file/9-3432/
* Google Scholar: https://scholar.google.de/ (mit VPN aufrufen!)
* Weitere Datenbank: https://www.sciencedirect.com/
